var routes = [],
	session = require('../module/session'),
	Mongo = require('../module/mongodb.js'),
	myError = require('../module/error'),
	db;

module.exports = function (app) {
	db = Mongo.get();
	for (var key in routes) {
		routes[key](app);
	}

	app.route('*').all(function (req, res) {
		res.send(myError.get(4004));
	})
};

routes.push(function (app) {
	try {
		require('../routes/auth')(app);
		console.log('- Route "/api/auth" added'.green);
	}
	catch (e) {
		console.log('- Route "/api/auth" failed...'.red);
	}
});

routes.push(function (app) {
	try {
		require('../routes/server')(app);
		console.log('- Route "/api/server" added'.green);
	}
	catch (e) {
		console.log('- Route "/api/server" failed...'.red);
	}
});