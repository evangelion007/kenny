var	errors = require('../dictionary/error');

// TO-DO :
// If the error is equal or greater than 
// 5000, log the error to error.log
// and maybe send an email ?

module.exports = {
	get: function (code) {
		if (!errors[code])
			code = 5000;
		return ({
			"error": {
				"code": code,
				"reason": errors[code]
			}
		});
	}
};