var _ = require('underscore'),
	URI = require('url'),
	net = require('net'),
	conf = require('../config/client')
	serverSchema = require('../schema/server'),
	request = require('request'),
	Mongo = require('../module/mongodb.js'),
	async = require('async'),
	APIKEY = "APIKEY",
	myError = require('../module/error'),
	db = Mongo.get(); // current mongodb connection

	function isKennyClient(err, res, callback, isKennyActive, opts)
	{
		if (!isKennyActive || err) return (callback(err, res));

		var port = conf['default_port'],
			hostname = opts.url.hostname;
		// console.log('http://'+hostname+":"+port+"/api/kenny?_apikey="+APIKEY);
		var latency = (new Date()).getTime();
		request({
			'url': 'http://'+hostname+":"+port+"/api/kenny?_apikey="+APIKEY,
			'method': 'GET'
		}, function (error, result) {
			res.kenny = false;
			if (error)
				res.kenny = false;
			else {
				try {
					result = JSON.parse(result.body);
					if (result.isKenny)
						res.kenny = true;
					delete result.isKenny;
					latency = (new Date()).getTime() - latency;
					result.latency = latency;
				} catch (e) {
					console.error(e);
					res.kenny = false;
				}
			}
			callback(err, res, result);
		});
	}

	module.exports = {
		getUrl: function (hostname) {
			if (_.isUndefined(hostname))
				return ('URL badly formated');
			if (hostname.indexOf('://') === -1)
				hostname = 'http://'+hostname;
			var url = URI.parse(hostname);
			if (!url.hostname)
				return (myError.get(4006));

			if (url.port == null)
				url.port = '80';
			return (url);
		},
		connect: function (hostname, callback, isKennyActive) {
			var socket = new net.Socket(),
				self = this,
				latency,
				url = this.getUrl(hostname),
				isKennyActive = isKennyActive || false,
				alreadySent = false;

			if (url.error) return (callback(url));
			// console.log(url);
			socket.on('error', function () {
				// console.log(arguments, url, hostname);
				if (alreadySent) return;
				alreadySent = true;
				self.getOne(hostname, function (err, result) {
					var res = {
						'alive': false
					};
					if (err == null)
						res['alreadySaved'] = result;
					isKennyClient(null, res, callback, isKennyActive || false, {
						'url': url
					});
					socket.end();
				});
			});

			socket.on('connect', function () {
				latency = (new Date()).getTime() - latency;
				if (alreadySent) return;
				alreadySent = true;
				self.getOne(hostname, function (err, result) {
					var res = {
						'alive': true,
						'latency': latency
					};
					if (err == null)
						res['alreadySaved'] = result;

					isKennyClient(null, res, 	callback,
					isKennyActive == true, {
						'url': url
					});
				});
				socket.end();
			});
			latency = (new Date()).getTime();
			socket.connect(url.port, url.hostname);
		},
		ping: function (id, callback) {
			var socket = new net.Socket(),
				self = this, latency,
				o_id = id,
				alreadySent = false;
			id = Mongo.TYPE.ObjectID(id);
			db.collection('servers', function (err, collection) {
				if (err) return (callback(myError.get(5000)));
				collection.findOne({
					'_id': id
				}, function (err, item) {
					if (err) return (callback(myError.get(5000)));
					if (!item) return (callback('Unable to find server with ID : "'+id+'".'));

					socket.on('error', function () {
						if (alreadySent) return;
						alreadySent = true;
						socket.end();
					});

					socket.on('connect', function () {
						latency = (new Date()).getTime() - latency;
						if (alreadySent) return;
						alreadySent = true;
						callback(null, {
							'id': o_id,
							'latency': latency,
							'href': item.address.href
						});
						socket.end();
					});

					latency = (new Date()).getTime();

					socket.connect(item.address.port, item.address.hostname);
				});
			});
		},
		save: function (url, callback) {
			url = this.getUrl(url);

			if (url.error) return (callback(myError.get(4006)));
			var server = serverSchema.get(url);
			db.collection('servers', function (err, collection) {
				if (err) {
					console.error(err);
					// throw myError.get(5000);
					return callback(myError.get(5000));
				}
				collection.findOne({
					'name': server.name
				}, function (err, res) {
					if (err) {
						// throw err;
						console.error(err);
						return callback(myError.get(5000));
					}
					// console.log(res, url);
					if (res) {
						return (callback(null, {
							'alreadyExist': true
						}));
					}
					collection.insert(server, function (err, item) {
						if (err) {
							console.error(err);
							return (callback(myError.get(5003), undefined))
						}
						return (callback(null, item));
					});
				});
			})
		},
		getAll: function (callback) {
			db.collection('servers', function (err, collection) {
				if (err) {
					console.error(err);
					return (callback(myError.get(5000)));
				}
				collection.find().toArray(function(err, servers) {
					if (err) {
						console.error(err);
						return (callback(myError.get(5000)));
					}
					servers = servers || [];
					_.each(servers, function (server) {
						server['id'] = server['_id'];
						server['_id'] = undefined;
					})
					callback(null, servers);
				});
			});
		},
		_checkStatus: function (servers, callback, self) {
			var exec_stack = [];

			_.each(servers, function (server) {
				exec_stack.push(function (asyncCB) {
					self.connect(server.address.host, function (err, res, kennyInfo) {
						if (!_.isObject(res))
							res = {};
						server.info.online = res.alive || false;
						server.info.kenny = res.kenny || false;
						server.info.latency = res.latency || 0;
						if (res.kenny) {
							server.info = _.extend(server.info, kennyInfo);
							return (asyncCB());
						}
						asyncCB();
					}, true);
				});
			});

			async.parallel(exec_stack, function () {
				callback(null, servers);
			});
		},
		getAllInformation: function (callback) {
			var self = this;
			db.collection('servers', function (err, collection) {
				if (err) {
					console.error(err);
					return (callback(myError.get(5000)));
				}
				collection.find().toArray(function(err, servers) {
					if (err) {
						console.error(err);
						return (callback(myError.get(5000)));
					}
					servers = servers || [];
					_.each(servers, function (server) {
						server['id'] = server['_id'];
						server['_id'] = undefined;
					})
					self._checkStatus(servers, callback, self);
				});
			});
		},
		getOne: function (url, callback) {
			url = this.getUrl(url);

			if (url.error) return (callback(myError.get(4006)));
			var server = serverSchema.get(url);
			db.collection('servers', function (err, collection) {
				if (err) {
					// throw err;
					console.error(err);
					return callback(myError.get(5000));
				}
				collection.findOne({
					'name': server.name
				}, function (err, res) {
					if (err) {
						// throw err;
						console.error(err);
						return callback(myError.get(5000));
					}
					return (callback(null, res ? true : false));
				});
			})
		}
	};