var crypto = require('crypto'),
	_ = require('underscore'),
	shasum = crypto.createHash('sha1'),
	sessions = {};

shasum.update(crypto.randomBytes(256));
var conf = {
	"tokenTimeout": 7 * 24 * 60 * 60 // 7 jour
};

module.exports = {
	salt: shasum.digest('hex'),
	hash: function (password, osalt) {
		if(osalt === undefined)
			osalt = this.salt;
		var shasum = crypto.createHash('sha1');
		shasum.update(password+osalt);
		return (shasum.digest('hex'));
	},
	connect: function (data) {
		var shasum = crypto.createHash('sha1'),
			token,
			self = this;
		shasum.update(crypto.randomBytes(256) + _.size(sessions));
		token = shasum.digest('hex');
		var user = sessions[token] = {
			'token': token,
			'session': data,
			'timeout': conf.tokenTimeout
		};
		user.connect = function () {
			self.connect(user.token);
		};

		user.disconnect = function () {
			delete sessions[user.token];
			delete user.token;
		};

		setTimeout(function () {
			user.disconnect();
		}, conf.tokenTimeout * 1000);
		return (user);
	},
	get: function (token) {
		return (sessions[token]);
	}
};