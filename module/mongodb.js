var MongoDbConfig = require(__dirname+'/../config/mongo.json'),
	MongoClient = require('mongodb').MongoClient,
	ObjectID = require('mongodb').ObjectID;

var MongoDB = {
	config: MongoDbConfig,
	TYPE: {
		ObjectID: function (id) {
			return (new ObjectID(id));
		}
	},
	connect: function (callback) {
		var self = this;
		MongoClient.connect(this.config.url, function(err, db) {
			self.__db = db;
			callback(err, db);
		});
	},
	get: function () {
		return (this.__db);
	},
	close: function () {
		if (!this.__db) return;
		this.__db.close();
		delete this.__db;
		if (this.__db)
			this.__db = undefined;
	}
};

MongoDB.__db = false;

module.exports = MongoDB;