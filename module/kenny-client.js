var express = require('express'),
	os = require('os'),
	app = express(),
	_ = require('underscore'),
	port = 2097,
	APIKEY="APIKEY";

app.use(function (req, res, next) {
	var apik = req.query['_apikey'];
	if (apik === APIKEY)
		return (next());
	res
	.status(400)
	.send({
		'error': {
			'code': 400,
			'reason': 'missing arguments...'
		}
	});
});

app.route('/api/kenny')
	.get(function (req, res) {
	res.send({
		'isKenny': true,
		'os': {
			'hostname': os.hostname(),
			'loadavg': os.loadavg(),
			'cpus': os.cpus(),
			'uptime': os.uptime(),
			'platform': os.platform(),
			'memory': {
				'total': os.totalmem(),
				'free': os.freemem()
			},
			'type': os.type()
		}
	});
});

module.exports = {
	start: function (opts) {
		if (_.isNumber(+opts['port']) && !_.isNaN(+opts['port']))
			port = opts['port'];
		app.listen(port);
		console.log('Kenny client is listening at port', port);
	}
};