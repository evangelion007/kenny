module.exports = {
	get: function (url) {
		var date = (new Date()).getTime();
		return ({
			"name": url.href,
			"configuration": {
				"active": true
			},
			"stat": {
				"create": date,
				"modify": 0
			},
			"info": {},
			"address": url
		});
	}
}