(function (App) {
	App.UI = {
		_loadTemplate: function (callback, ctx) {
			Backbone.Template.get('application.html', function (err, html) {
				if (err) alert(err);
				callback.apply(ctx || this, [$(html)]);
			});
			return (this);
		},
		load: function (AppCB, App) {
			this._loadTemplate(function ($html) {
				var $parent = App.get$Content().parent();
				App.get$Content().remove();
				$parent.append($html);
				App.$content = $html.find('.page-location');
				AppCB.apply(App, []);
				App.router.initLink();
			}, this);
		}
	};
})(window.App);