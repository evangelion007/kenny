(function () {
	App.Pages.Login = Backbone.Page.extend({
		init: function () {
			this.on('show', this.render, this);
		},
		events: {
			"submit form": "login"
		},
		saveReference: function () {
			this.$username = this.$('[name="username"]');
			this.$password = this.$('[name="password"]');
			this.$errorBox = this.$('.panel-fix.errorBox');
			this.$errorBoxMsg = this.$('.panel-fix.errorBox .panel-body');
			this.$errorBox.stop().hide(0);
		},
		loadTemplate: function (callback, ctx) {
			Backbone.Template.get('login.html', function (err, html) {
				if (err) alert(err);
				callback.apply(ctx || this, [html]);
			});
			return (this);
		},
		render: function () {
			var $el = this.$el;
			this.undelegateEvents();
			this.loadTemplate(function (html) {
				this.$el = $(html);
				$el.remove();
				this.delegateEvents();
				this.getApp()
					.get$Content()
					.append(this.$el);
				this.saveReference();
			}, this);
			return (this);
		},
		login: function (e) {
			if (e && _.isFunction(e.preventDefault))
				e.preventDefault();
			this.$errorBox.stop().hide(0);
			Backbone.Network.postJSON({
				'url': '/api/auth',
				'data': {
					"username": this.$username.val(),
					"password": this.$password.val()
				}
			}, this, function (err, res) {
				if (err) {
					this.$errorBoxMsg
						.html('A technical error has occured, for more information go to see the log.')
					this.$errorBox
						.stop()
						.show(0);
					console.log(err, res);
					return;
				}
				if (res.error) {
					this.$errorBoxMsg
						.html(res.error.reason)
					this.$errorBox
						.stop()
						.show(0);
				}
				else {
					this.getApp()
						.Cache
							.set('profile', res.profile)
							.set('token', res.token);
					this.getApp().startApplication(res);
					console.log('connected');
				}
			});
		},
		start: function () {
			this.render();
			return (this);
		}
	});
})();