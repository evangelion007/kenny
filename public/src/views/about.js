(function () {
	App.Pages.About = Backbone.Page.extend({
		init: function () {
			this.$menus = $('[data-view-menu]');
			this.$menu = $('[data-view-menu="About"]');
			this.getEvents().on('show', this._show, this);
		},
		loadTemplate: function (callback, ctx) {
			Backbone.Template.get('about.html', function (err, html) {
				if (err) alert(err);
				callback.apply(ctx || this, [html]);
			});
			return (this);
		},
		render: function (callback) {
			var $el = this.$el;
			this.undelegateEvents();
			this.loadTemplate(function (html) {
				this.$el = $(html);
				$el.remove();
				this.delegateEvents();
				this.getApp()
					.get$Content()
					.append(this.$el);
				callback.apply(this, []);
			}, this);
			return (this);
		},
		start: function (callback) {
			this.render(callback);
			return (this);
		},
		_show: function () {
			this.$menus.removeClass('active');
			this.$menu.addClass('active');
		}
	});
})();