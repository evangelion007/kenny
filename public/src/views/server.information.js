(function () {
	App.Pages.ServerInformation = Backbone.Page.extend({
		config: {
			'interval': 2 // seconde
		},
		init: function () {
			this.getApp().getEvents().on('servers:update', this.drawServerInformation, this);
		},
		events: {
		},
		saveReference: function () {
			this.$name = this.$('[data-target="name"]');
			this.$hostname = this.$('[data-target="hostname"]');
			this.$kenny = this.$('[data-target="kenny"]');
			this.$status = this.$('[data-target="status"]');
			this.$latency = this.$('[data-target="latency"]');
			this.$progressBars = this.$('[data-target="progress-bars"]');
			this._progressTmp = this.$('[data-template="progress"]').html();
		},
		loadTemplate: function (callback, ctx) {
			Backbone.Template.get('server.information.html', function (err, html) {
				if (err) alert(err);
				callback.apply(ctx || this, [html]);
			});
			return (this);
		},
		setArguments: function (id) {
			this.__id = id;
		},
		render: function (callback) {
			var $el = this.$el;
			this.undelegateEvents();
			this.loadTemplate(function (html) {
				this.$el = $(html);
				$el.remove();
				this.delegateEvents();
				this.getApp()
					.get$Content()
					.append(this.$el);
				this.saveReference();
				this.drawServerInformation();
				callback.apply(this, []);
			}, this);
			return (this);
		},
		start: function (callback) {
			this.render(callback);
			return (this);
		},
		getServerId: function () {
			return (this.__id);
		},
		getServerList: function () {
			return (this.getApp().pages.Homepage.getServerList());
		},
		getServer: function (n) {
			var id = this.getServerId();
			var elm = _.find(this.getServerList(), function (server) {
				return (server.id === id);
			});
			return (elm);
		},
		getCpuUsage: function (cpu) {
			//Initialise sum of idle and time of cores and fetch CPU info
			var totalIdle = 0, totalTick = 0;

			//Total up the time in the cores tick
			for(type in cpu.times) {
				totalTick += cpu.times[type];
			}     

			//Total up the idle time of the core
			totalIdle += cpu.times.idle;

			//Return the average Idle and Tick times
			return ({
					idle: totalIdle,
					total: totalTick
				});
		},
		drawServerInformation: function (server) {
			var server = this.getServer() || {};

			this.$name.html(server.address.href);
			this.$hostname.html(server.info.os.hostname);

			if (server.info.kenny)
				this.$kenny.html('<span class="label label-success col-xs-3" style="display:block;text-align:center;">detected</span>');
			else
				this.$kenny.html('<span class="label label-success col-xs-3" style="display:block;text-align:center;">detected</span>');

			if (server.info.online)
				this.$status.html('<span class="label label-success col-xs-3" style="display:block;text-align:center;">online</span>');
			else
				this.$status.html('<span class="label label-success col-xs-3" style="display:block;text-align:center;">offline</span>');

			this.$latency.html(server.info.latency+' ms');

			this.$progressBars.html('');
			_.each(server.info.os.cpus, function (cpu) {
				var $html = $(this._progressTmp);
				var $progress = $html.find('.progress-bar');
				console.log('==>', cpu);
				var consumption = this.getCpuUsage(cpu);
				var total = +((100 - (+(consumption.idle / consumption.total) * 100)).toFixed(2));
				$html.find('[data-target="name"]').html(cpu.model);
				$html.find('[data-target="speed"]').html((cpu.speed / 1000).toFixed(2) + "Ghz");
				$progress.attr('aria-valuenow', total);
				$progress.css('width', total+'%');
				console.log(total);
				$progress.html(total);
				this.$progressBars.append($html);
			}, this);
			// this.$progress.html(total);
			// this.$progress.attr('aria-valuenow', total);
			// this.$progress.css('width', total+'%');
			// this.$el.html(JSON.stringify(server, 2, '<br/>'));
		}
	});
})();