(function () {
	App.Pages.Homepage = Backbone.Page.extend({
		config: {
			'interval': 2 // seconde
		},
		init: function () {
			this.$menus = $('[data-view-menu]');
			this.$menu = $('[data-view-menu="Homepage"]');
			this.getEvents().on('show', this._show, this);
			this.getEvents().on('hide', this._hide, this);
			this.getEvents().on('servers:update', this.drawServerInformation, this);
		},
		events: {
			'click [data-action="goToAddServer"]': 'addServer',
			'click [data-target-id]': 'goToServerInformation'
		},
		saveReference: function () {
			this.serverTmpl = this.$('#templateServer').html();
			this.$serverList = this.$('tbody.server-list');
		},
		addServer: function () {
			this.getApp()
				.goTo('Server.New');
		},
		loadTemplate: function (callback, ctx) {
			Backbone.Template.get('homepage.html', function (err, html) {
				if (err) alert(err);
				callback.apply(ctx || this, [html]);
			});
			return (this);
		},
		render: function (callback) {
			var $el = this.$el;
			this.undelegateEvents();
			this.loadTemplate(function (html) {
				this.$el = $(html);
				$el.remove();
				this.delegateEvents();
				this.getApp()
					.get$Content()
					.append(this.$el);
				this.saveReference();
				callback.apply(this, []);
			}, this);
			return (this);
		},
		getInformationRoutine: function () {
			if (!this.__routineIsActive) return;
			Backbone.Network.getJSON({
				'url': this.getApp().getApiKeyUrl('/api/servers')
			}, this, function (err, res) {
				if (res.servers) {
					var lastDate = (new Date()).getTime();
					this.lastDate = lastDate;
					_.each(res.servers, function (server) {
						server.info.os = server.info.os || {};
					})
					this.getEvents().trigger('servers:update', res.servers, lastDate);
					this.getApp().getEvents().trigger('servers:update', res.servers, lastDate);
				}
			});
		},
		goToServerInformation: function (e) {
			var $tr = $(e.target),
				limit = 10;
			while (!$tr.attr('data-target-id') && (--limit > 0)) {
				console.log(!$tr.attr('data-target-id'), (--limit > 0), !$tr.attr('data-target-id') && (--limit > 0));
				$tr = $tr.parent();
			}
			if (limit == 0)
				return; // Error
			console.log($tr.attr('data-target-id'));
			this.getApp().goTo('Server.Information', {
				'id': $tr.attr('data-target-id')
			});
		},
		getServerList: function () {
			return (this.__servers);
		},
		drawServerInformation: function (servers, time) {
			var serverList = '';
			this.undelegateEvents();
			this.$serverList.html(serverList); // emptying the content ''
			this.delegateEvents();
			this.__servers = servers;
			_.each(servers, function (server) {
				console.log(server);
				serverList += Template(this.serverTmpl, {
					"server": server
				});
			}, this);

			if (this.lastDate === time)
				this.$serverList.html(serverList);
			if (this.lastDate === time && this.__routineIsActive) {
				var self = this;
				setTimeout(function () {
					self.getInformationRoutine();
				}, this.config.interval * 1000);
			}
		},
		startRoutine: function () {
			var self = this;
			this.__routineIsActive = true;
			this.getInformationRoutine();
		},
		stopRoutine: function () {
			this.__routineIsActive = false;
		},
		start: function (callback) {
			this.render(callback);
			return (this);
		},
		_show: function () {
			this.$menus.removeClass('active');
			this.$menu.addClass('active');
			this.getInformationRoutine();
			this.startRoutine();
		},
		_hide: function () {
			// this.stopRoutine();
		}
	});
})();