(function () {
	App.Pages.ServerNew = Backbone.Page.extend({
		init: function () {
			this.$menus = $('[data-view-menu]');
			this.getEvents().on('show', this._show, this);
		},
		events: {
			'submit form': 'addServer',
			'click [data-action="save-server"]': 'saveServer',
			'click [data-action="submit"]': 'addServer'
		},
		saveReference: function () {
			this.$iserver = this.$('[name="server_addr"]');
			this.$messageFull = this.$('[data-message="checklist-full"]');
			this.$messageHalf = this.$('[data-message="checklist-half"]');
			this.$messageFailed = this.$('[data-message="checklist-failed"]');
			this.$messageAlreadtSaved = this.$('[data-message="checklist-already-saved"]');
			this.$messageBtnAdd = this.$('[data-message="add_server"]');
			this.$('[data-message]').hide(0);
		},
		saveServer: function (e) {
			if (e && _.isFunction(e.preventDefault))
				e.preventDefault();
			this.$('[data-message]').hide(0);
			Backbone.Network.postJSON({
				'url': '/api/servers',
				'data': {
					'serverAddress': this.$iserver.val()
				}
			}, this, function (err, res) {
				var server = res.server;
				if (_.isBoolean(server.save)) {
					return this.getApp().goTo('Homepage');
				}
				alert('An error is occurend, check the log.');
				console.error(err, res);
			});
		},
		addServer: function (e) {
			if (e && _.isFunction(e.preventDefault))
				e.preventDefault();

			this.$iserver.attr('readonly', 'readonly');
			this.$('[data-message]').hide(0);

			Backbone.Network.getJSON({
				'url': '/api/server/ping',
				'data': {
					'serverAddress': this.$iserver.val()
				}
			}, this, function (err, res) {
				if (err) return (this.$iserver.removeAttr('readonly'));
				
				var server = res.server || {};

				if (server.alive && server.kenny)
					this.$messageFull.stop().show(0);
				else if (server.alive && !server.kenny)
					this.$messageHalf.stop().show(0);
				else {
					this.$messageFailed.stop().show(0);
				}
				if (server.alreadySaved) {
					this.$messageAlreadtSaved.show(0);
				}
				this.$messageBtnAdd.stop().show(0);
				this.$iserver.removeAttr('readonly');
			});
		},
		loadTemplate: function (callback, ctx) {
			Backbone.Template.get('server.new.html', function (err, html) {
				if (err) alert(err);
				callback.apply(ctx || this, [html]);
			});
			return (this);
		},
		render: function (callback) {
			var $el = this.$el;
			this.undelegateEvents();
			this.loadTemplate(function (html) {
				this.$el = $(html);
				$el.remove();
				this.delegateEvents();
				this.getApp()
					.get$Content()
					.append(this.$el);
				this.saveReference();
				callback.apply(this, []);
			}, this);
			return (this);
		},
		start: function (callback) {
			this.render(callback);
			return (this);
		},
		_show: function () {
			this.$menus.removeClass('active');
			this.$('[data-message]').hide(0);
		}
	});
})();