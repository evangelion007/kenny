(function () {
	window.Template = function (content, obj) {
		content = _.template(content || '');
		return (content(obj || {}));
	};

	var url = location.protocol;
	url += "//"+location.host;
	if (location.href != url+"/") {
		console.log(url, location.href);
		location.href = url;
		return;
	}
	// Network configuration
	Backbone.Network.setUrl(url);
	Backbone.Template.setUrl(url+'/templates/');

	// Start Application
	Backbone.Application
		.attachApp(window.App, "run")
		.start();
})();