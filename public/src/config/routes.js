(function (App) {
	var routesProto = [{
		"url": "",
		"name": "Homepage",
		"viewName": "Homepage"
	},{
		"url": "about",
		"name": "About",
		"viewName": "About"
	},{
		"url": "server/new",
		// "url": "",
		"name": "Server.New",
		"viewName": "ServerNew"
	},{
		"url": "server/information/:id",
		"name": "Server.Information",
		"viewName": "ServerInformation"
	}];

	var routes = {
		initialize: function (opts) {
			this.__app = opts.app;
		},
		routes: {},
		getApp: function () {
			return (this.__app);
		},
		initLink: function () {
			var self = this;
			$('body').on('click', 'a, area', function (e) {
				if ($(this).hasClass('no-follow')) return;
				e.preventDefault();
				var href = $(this).attr('href');
				self.navigate(href, {
					'trigger': true,
					'replace': true
				});
			});
		}
	};

	_.each(routesProto, function (route) {
		routes['routes'][route.url] = route.name;
		routes[route.name] = function () {
			if (!this.getApp().pages[route.viewName]) {
				this.getApp().pages[route.viewName] = new (this.getApp().Pages)[route.viewName]({
					'app': this.getApp()
				});
				if (this.getApp().pages[route.viewName].setArguments) {
					this.getApp().pages[route.viewName].setArguments.apply(this.getApp().pages[route.viewName], arguments);
				}
				this.getApp().pages[route.viewName].start(function () {
					this.show();
				});
				return;
			}
			if (this.getApp().pages[route.viewName].setArguments) {
				this.getApp().pages[route.viewName].setArguments.apply(this.getApp().pages[route.viewName], arguments);
			}
			App.pages[route.viewName].show();
		};
	}, this);
	routes['routes']['*notFound'] = 'routeNotFound';
	routes['routeNotFound'] = function () {
		console.log(arguments);
	};
	App.Cache.set('Router', routes);
	App.Cache.set('Routes', routesProto);
})(window.App);