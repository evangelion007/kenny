window.App = {
	Pages: {},
	Views: {},
	pages: {},
	Config: {
		_conf: {
			'APIKEY': 'kenny_apikey'
		},
		get: function (key) {
			return (this._conf[key]);
		}
	},
	Cache: {
		cache: {},
		set: function (key, value) {
			this.cache[key] = value;
			return (this);
		},
		get: function (key) {
			return (this.cache[key]);
		}
	},
	getApiKeyUrl: function (url, key) {
		if (!key) {
			key = this.Cache.get('token');
		}
		return (url+'?_apikey='+key);
	},
	goTo: function (name, args) {
		var route = _.find(this.Cache.get('Routes'), function (route) {
			return (route.name === name);
		});
		if (route) {
			route = route.url;
			if (args) {
				_.each(args, function (arg, key) {
					route = route.replace(':'+key, arg);
				});
			}
		}
		else {
			route = ""+route;
		}
		// console.log(route);
		this.router.navigate(route, {
			'trigger': true,
			'replace': true
		});
	},
	$content: $('#content'),
	get$Content: function () {
		return (this.$content);
	},
	getEvents: function () {
		return (this.__events);
	},
	run: function () {
		window.App = undefined; // remove reference
		this.checkLogin();
	},
	checkLogin: function () {
		var apikey = Backbone.Cookie.get(this.Config.get('APIKEY'));
		if (!apikey) {
			return (this.startLogin());
		}
		Backbone.Network.getJSON({
			'url': this.getApiKeyUrl('/api/auth', apikey.getValue())
		}, this, function (err, res) {
			if (err) return;
			if (!(res.token && res.profile)) {
				apikey.remove(); // delete the cookie
				return (this.startLogin());
			}

			this.Cache
				.set('profile', res.profile)
				.set('token', res.token);
			this.startApplication();
		});
	},
	startApplication: function (user) {
		if (user) {
			var cookie = Backbone.Cookie.new(this.Config.get('APIKEY'));
			cookie.setValue(user.token);
			cookie.save();
		}
		this.__events = _.extend({}, Backbone.Events);
		this.UI.load(function () {
			this.router = new this.Router({
				app: this
			});
			Backbone.history.start();
			// console.log('app started', this.router);
			this.goTo('Homepage');
		}, this);
	},
	startLogin: function () {
		this.pages.Login = (
			new this.Pages.Login({
				app: this
			})
		).start();

	}
};