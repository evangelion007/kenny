var server = require('../module/server'),
	myError = require('../module/error'),
	_ = require('underscore');

module.exports = function (app) {
	app
	.route('/api/server/ping')
	.get(function (req, res) {
		var hostname = req.query['serverAddress'];
		try {
			server.connect(hostname, function (err, result) {
				if (err) 
					return (res.send(err));
				res.send({
					"server": result
				});
			}, true);
		} catch (e) {
			console.error(e);
			res.send({
				'error': {
					'code': 500,
					'reason': 'Internal server error.'
				}
			});
		}
	});

	app
	.route('/api/server/ping/only')
	.get(function (req, res) {
		var id = req.query['id'];
		if (_.isUndefined(id))
			return (res.send(myError.get(4001)));
		server.ping(id, function (err, result) {
			if (err) return (res.send(err));
			res.send(result);
		});
	});

	app
	.route('/api/servers')
	.post(function (req, res) {
		var hostname = req.body['serverAddress'];
		if (_.isUndefined(hostname)) {
			return (res.send(myError.get(4001)));
		}
		try {
			server.save(hostname, function (err, result) {
				if (err) return (res.send(err));
				return (
					res.send({
						'server': {
							'save': (result.alreadyExist ? false : true)
						}
					})
					);
			})
		} catch (e) {
			console.error(e);
			return (res.send(myError.get(5000)));
		}
	})
	.get(function (req, res) {
		server.getAllInformation(function (err, items) {
			if (err) return (res.send(err));
			return (
				res.send({
					'servers': items
				})
			);
		});
	});
};