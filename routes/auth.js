var	session = require('../module/session'),
	Mongo = require('../module/mongodb.js'),
	myError = require('../module/error'),
	db;

module.exports = function (app) {
	db = Mongo.get();
	app	.route('/api/auth')
	.post(function (req, res) {
		var username = req.body.username || '',
		password = req.body.password || '';
		db.collection('accounts', function (err, collection) {
			if (err)
				return res.send(myError.get(5000));
			collection.findOne({
				'username': username
			}, function (err, item) {
				if (err)
					return res.send(myError.get(5000));
				if (item) {
					var pass = session.hash(password, item.salt);
					if (item.password === pass) {
						var user = JSON.newObject(item);
						user = session.connect(user);
						delete item.salt;
						delete item.password;
						item.id = item._id;
						delete item._id;
						return (
							res.send({
								'profile': item,
								'token': user.token
							})
						);
					}
				}
				res.send(myError.get(4000));
			});
		})
	})
	.get(function (req, res) {
		var token = req.query['_apikey'];
		if (!token) {
			return res.send(myError.get(8000));
		}
		var user = session.get(token);
		if (!user) {
			return res.send(myError.get(8001));
		}
		var item = JSON.newObject(user.session || {});
		item.id = item._id;
		delete item.salt;
		delete item.password;
		delete item._id;
		return (
			res.send({
				'profile': item,
				'token': user.token
			})
			);
	});
};