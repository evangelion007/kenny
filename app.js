var colors = require('colors'),
	express = require('express'),
	packageJson = require('./package.json'),
	app = express(),
	Mongodb = require('./module/mongodb.js'),
	_ = require('underscore'),
	port = 2000;

// Express Module
var bodyParser = require('body-parser'),
	compress = require('compression'),
	methodOverride = require('method-override'),
	errorHandler = require('errorhandler');


JSON.newObject = function (obj) {
	return (JSON.parse(JSON.stringify(obj)));
};

app.route('/api/version')
.get(function (req, res) {
	res.send({
		"version": packageJson.version,
		"powered-by": packageJson.name
	});
});

try {
	console.log("Modules initialization...".yellow);
	app.use(compress());
	app.use(methodOverride());
	app.use(bodyParser.urlencoded({
		"extended": true
	}));
	app.use(bodyParser.json());
	app.use(express.static(__dirname+'/public'))
	app.use(errorHandler());
	console.log("OK".green);

	console.log("MongoDb initialization...".yellow);
	Mongodb.connect(function (err, res) {
		if (err) throw "Mongodb can't be initialize. Critical Error.";
		console.log("OK".green);
		console.log("Route initialization".yellow);
		require('./module/route')(app);
		console.log("OK".green);
		app.listen(port);
		console.log(("Server is now listening at port "+port).green);

		process.on('SIGINT', function(code) {
			if (_.isUndefined(code))
				code = 0;
			var db = Mongodb.get();
			if (_.isObject(db) && _.isFunction(db.close)) {
				console.log('Closing connection of MongoDb'.green);
				db.close()
			}
			console.log('About to exit with code:', code);
			process.exit(code);
		});
	});
} catch (e) {
	console.error("An error occured".red);
	console.error(e);
}