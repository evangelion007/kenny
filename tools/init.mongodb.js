var MongoDbConfig = require(__dirname+'/../config/mongo.json'),
	MongoClient = require('mongodb').MongoClient,
	colors = require('colors'),
	_ = require('underscore'),
	async = require('async'),
	done = 0;

var collections = ['accounts', 'servers', 'configuration'];

console.log('Initialize connection to MongoDB...'.blue);

MongoClient.connect(MongoDbConfig.url, function(err, db) {
	if (err) {
		console.log('Impossible to connect to MongoDB, please check the configuration file'.red);
		db.close();
		throw err;
	}
	console.log('Connection to MongoDB has been successfully done.'.green);
	console.log('MongoDB will create theses collections : ');
	var exec_stack = [];
	_.each(collections, function (name) {
		console.log('-', name);
		exec_stack.push(function (asyncCB) {
			db.createCollection('accounts', {
				'autoIndexId': true
			}, function (err) {
				if (err) {
					console.error(('(-) '+name+' has not been created').red);
					try {
						db.close();
						throw err;
					} catch (e) {
						console.error('cannot close.');
					}
				}
				++done;
				console.log(('(+) '+name+' has been created').green);
				db.collection(name, function (err, collection) {
					if (err) {
						console.error(err.red);
						return db.close();
						throw err;
						// return asyncCB();
					}
					collection.remove({});
					asyncCB();
				});
			});
		});	
	});
	console.log('Processing...'.blue);
	async.parallel(exec_stack, function () {
		if (done === collections.length)
			console.log('SUCCESS'.green);
		else
			console.error(('FAILED! ('+done+'/'+collections.length+')').red);
		console.log('Close connection..'.blue);
		db.close();
		console.log('Done'.green);
	});
});