var MongoDbConfig = require(__dirname+'/../config/mongo.json'),
	MongoClient = require('mongodb').MongoClient,
	colors = require('colors'),
	_ = require('underscore'),
	async = require('async'),
	session = require('../module/session')
	done = 0;

var firstname = process.argv[2],
	lastname = process.argv[3],
	username = process.argv[4],
	password = process.argv[5];

if (_.isUndefined(firstname) || _.isUndefined(lastname) || _.isUndefined(username) || _.isUndefined(password)) {
	console.log(__filename+': firstname lastname username password');
	return (-1);
}

console.log('First name: ', firstname);
console.log('Last name: ', lastname);
console.log('Username: ', username);
var password = {
	'password': session.hash(password),
	'salt': session.salt
};
console.log('Password (salt): ', password.password+'('+password.salt+')');
console.log('You have 3 seconds to cancel'.green);

setTimeout(function () {
	MongoClient.connect(MongoDbConfig.url, function(err, db) {
		if (err) {
			console.log('Impossible to connect to MongoDB, please check the configuration file'.red);
			db.close();
			throw err;
		}
		console.log('Connection to MongoDB has been successfully done.'.green);
		console.log('Processing...'.blue);
		db.collection('accounts', function (err, collection) {
			collection.insert({
				"firstname": firstname,
				"lastname": lastname,
				"username": username,
  				"password": password.password,
				"salt": password.salt
			}, function (err) {
				if (!err)
					console.log('SUCCESS'.green);
				else
					console.error(('FAILED! ('+done+'/'+collections.length+')').red);
				console.log('Close connection..'.blue);
				db.close();
				console.log('Done'.green);
			});
		});
	});
}, 3000);