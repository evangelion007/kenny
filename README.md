# Kenny

Kenny has been designed for monitoring information from other servers.


## Requirements
Kenny use MongoDB and Node.js, you have to have both of them in order to run kenny-server, but you only need Node.js for the instance of Kenny.

## Install

In order to initialize the database, you just need to execute ```npm install```.

And then,for creating an account you have to go to the ```tools/``` folder and execute ```node add_user.js firstname lastname username password```.

## Tools

You can go to the folder ```tools/``` in order to execute some script to configure the server, add user, etc...
